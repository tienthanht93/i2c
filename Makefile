.PHONY: all clean

CROSS_COMPILE=gcc
C_SRC=udoo_i2c_dev.c main.c
C_DIR=.
CFLAGS += -std=gnu99
CFLAGS += -I./include

PROGRAM=test_i2c
all: $(PROGRAM)

$(PROGRAM): $(C_SRC:.c=.o)
	$(CROSS_COMPILE) -o $@ $^ $(LIBS)

$(C_SRC:.c=.o): %.o: $(C_DIR)/src/%.c
	$(CROSS_COMPILE) -c -o $@ $^ $(CFLAGS) $(LIBS)

clean:
	rm -rf *.o
	rm -rf $(PROGRAM)
