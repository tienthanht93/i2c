#include <stdio.h>
#include "fxas21002c.h"
#include "udoo_i2c_dev.h"
#include <unistd.h>
#include <string.h>

#define SENSITIVITY 0.244f
#define G_CONSTANT 9.81f

double getG(int value)
{
    double ret_val = ((value * SENSITIVITY)/1000)*G_CONSTANT;
    return ret_val;
}
int main()
{
    int i2cbus = 3; //for accelerometer and magnetometer
    char filename[30];
    char buffer[6];
    int fd = udoo_open_i2c_dev(i2cbus, filename, FXOS8700CQ_SLAVE_ADDR);
    sensor_data_t accelerometer;
    sensor_data_t magnetometer;
    /*
    while (1)
    {
        char control_data = 0;
        udoo_i2c_read_byte(fd, &control_data, 0x0E);
        printf("control data: %x\n", control_data);
        usleep(300*1000);
    }
    */
    udoo_i2c_disable_accel(fd);
    udoo_i2c_enable_accel(fd);
    while (1)
    {
        memset(buffer, 0, sizeof(buffer));
        udoo_i2c_read_accel(fd, buffer);

        accelerometer.x = (short)(((buffer[0] & 0xFF) << 8) | (buffer[1]) & 0xFF) >> 2;
        accelerometer.y = (short)(((buffer[2] & 0xFF) << 8) | (buffer[3]) & 0xFF) >> 2;
        accelerometer.z = (short)(((buffer[4] & 0xFF) << 8) | (buffer[5]) & 0xFF) >> 2;

        memset(buffer, 0, sizeof(buffer));
        udoo_i2c_read_mag(fd, buffer);
        magnetometer.x = (short)(((buffer[0] & 0xFF) << 8) | (buffer[1]) & 0xFF) >> 2;
        magnetometer.y = (short)(((buffer[2] & 0xFF) << 8) | (buffer[3]) & 0xFF) >> 2;
        magnetometer.z = (short)(((buffer[4] & 0xFF) << 8) | (buffer[5]) & 0xFF) >> 2;

        printf("accelerometer, %0.2f m/s2 , %0.2f m/s2 , %0.2f m/s2\n", getG(accelerometer.x), getG(accelerometer.y), getG(accelerometer.z));
        printf("magnetometer, %d , %d , %d\n", magnetometer.x, magnetometer.y, magnetometer.z);
        usleep(300*1000);
    }
    return 0;
}
