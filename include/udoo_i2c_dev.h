/* authors              date(mm/dd/yyyy)        notes
 * ThanhNT              02/10/2017              Initial version
 */
#ifndef __UDOO_I2C_DEV_H__
#define __UDOO_I2C_DEV_H__

#include <sys/ioctl.h>
#include <stdio.h>
#include <fcntl.h> 
#include <errno.h>

#define UDOO_I2C_READ	1
#define UDOO_I2C_WRITE	0

// Device-dependent request code 
#define I2C_SMBUS	0x0720	// SMBus-level access

typedef struct udoo_i2c_cmd_s{
	char read_write;
	char register_addr;
	int size;
	char *data;
} udoo_i2c_cmd_t;
int udoo_i2c_read_byte(const int fd, char *buffer, int register_addr);
int udoo_open_i2c_dev(const int i2cbus, char *filename, int dev_addr);
int udoo_i2c_read_mag(const int fd, char buffer[]);
int udoo_i2c_read_accel(const int fd, char buffer[]);
int udoo_i2c_disable_accel(const int fd);
int udoo_i2c_enable_accel(const int fd);
#endif
